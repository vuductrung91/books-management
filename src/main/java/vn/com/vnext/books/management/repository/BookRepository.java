package vn.com.vnext.books.management.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import vn.com.vnext.books.management.model.Book;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    Page<Book> findByPublished(boolean published, Pageable pageable);

    @Query("SELECT t FROM Book t WHERE t.title LIKE CONCAT('%',:searchTerm, '%')")
    Page<Book> findByTitleContaining(@Param("searchTerm") String title, Pageable pageable);

    @Query("SELECT t FROM Book t WHERE t.author LIKE CONCAT('%',:searchTerm, '%')")
    Page<Book> findByAuthorContaining(@Param("searchTerm") String author, Pageable pageable);

    List<Book> findByTitleContaining(String title, Sort sort);
}
